# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160730230612) do

  create_table "cabecalhos", force: :cascade do |t|
    t.string   "indentificacao_documento"
    t.string   "filler"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "complementonotafiscals", force: :cascade do |t|
    t.string   "tipo_periodo_entrega"
    t.string   "data_inicial_entrega"
    t.string   "hora_inicial_entrega"
    t.string   "data_final_entrega"
    t.string   "hora_final_entrega"
    t.string   "uso_cliente"
    t.string   "natureza_nf"
    t.string   "tipo_entrega"
    t.string   "tipo_carga"
    t.string   "origem"
    t.string   "filler"
    t.string   "observacao_conhecimento"
    t.string   "pedido"
    t.string   "aliquota_icms"
    t.string   "base_calculo_icms"
    t.string   "cfop_nf"
    t.string   "referencia_entrega"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "consignatarios", force: :cascade do |t|
    t.string   "razao_social"
    t.string   "cnpj"
    t.string   "inscricao_estadual"
    t.string   "endereco"
    t.string   "bairro"
    t.string   "cidade"
    t.string   "cep"
    t.string   "codigo_municipio"
    t.string   "uf"
    t.string   "numero"
    t.string   "filler"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  create_table "dadoscomplementares", force: :cascade do |t|
    t.string   "email"
    t.string   "celular"
    t.string   "tipo_estabelecimento"
    t.string   "contribuinte"
    t.string   "pais"
    t.string   "inscricao_suframa"
    t.string   "validade_insc_suframa"
    t.string   "filler"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  create_table "dadosnota", force: :cascade do |t|
    t.string   "romaneio_coleta"
    t.string   "cod_rota"
    t.string   "meio_transporte"
    t.string   "tipo_transporte"
    t.string   "tipo_carga"
    t.string   "condicao_frete"
    t.string   "serie"
    t.string   "numero"
    t.string   "data_emissao"
    t.string   "natureza_mercadoria"
    t.string   "especie_acionamento"
    t.string   "volume"
    t.string   "valor_mercadoria"
    t.string   "peso_total"
    t.string   "peso_densidade_cubagem"
    t.string   "incidencia_icms"
    t.string   "seguro_efetuado"
    t.string   "valor_seguro"
    t.string   "valor_a_ser_cobrado"
    t.string   "placa_caminhao"
    t.string   "plano_carga_rapida"
    t.string   "valor_frete_peso_volume"
    t.string   "valor_ad_valorem"
    t.string   "valor_total_taxas"
    t.string   "valor_total_frete"
    t.string   "acao_documento"
    t.string   "valor_icms"
    t.string   "valor_icms_retido_subst"
    t.string   "inidicacao_bonificacao"
    t.string   "filler"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "dadosredespachos", force: :cascade do |t|
    t.string   "razao_social"
    t.string   "cnpj"
    t.string   "inscricao_estadual"
    t.string   "endereco"
    t.string   "bairro"
    t.string   "cidade"
    t.string   "cep"
    t.string   "codigo_municipio"
    t.string   "uf"
    t.string   "area_frete"
    t.string   "numero_comunicacao"
    t.string   "filler"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  create_table "destinatarios", force: :cascade do |t|
    t.string   "razao_social"
    t.string   "cnpj"
    t.string   "inscricao_estadual"
    t.string   "endereco"
    t.string   "bairro"
    t.string   "cidade"
    t.string   "cep"
    t.string   "codigo_municipio"
    t.string   "uf"
    t.string   "area_frete"
    t.string   "numero_comunicacao"
    t.string   "indentificacao_destinatario"
    t.string   "filler"
    t.string   "cep_or_zip_code"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "intercambios", force: :cascade do |t|
    t.string   "remetente"
    t.string   "destinatario"
    t.string   "data"
    t.string   "hora"
    t.string   "nome_arquivo"
    t.string   "filler"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "localcoleta", force: :cascade do |t|
    t.string   "cnpj"
    t.string   "razao_social"
    t.string   "inscricao_estadual"
    t.string   "endereco_solicitante"
    t.string   "complemento_endereco"
    t.string   "bairro"
    t.string   "cidade"
    t.string   "uf"
    t.string   "pais"
    t.string   "cep"
    t.string   "telefone_fixo"
    t.string   "fax"
    t.string   "telefone_movel"
    t.string   "email"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "mercadoria_nota_fiscals", force: :cascade do |t|
    t.string   "quantidade_volumes"
    t.string   "especie_acionamento"
    t.string   "mercadoria_nota_fiscal"
    t.string   "quantidade_volumes2"
    t.string   "especie_acionamento2"
    t.string   "mercadoria_nota_fiscal2"
    t.string   "quantidade_volumes3"
    t.string   "especie_acionamento3"
    t.string   "mercadoria_nota_fiscal3"
    t.string   "quantidade_volumes4"
    t.string   "especie_acionamento4"
    t.string   "mercadoria_nota_fiscal4"
    t.string   "filler"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "redespachos", force: :cascade do |t|
    t.string   "cnpj_emissio"
    t.string   "sigla_fiscal"
    t.string   "numero"
    t.string   "serie"
    t.string   "data_emissao"
    t.string   "total_frete"
    t.string   "frete_peso"
    t.string   "frete_valor"
    t.string   "valor_base_calculo_icms"
    t.string   "valor_icms"
    t.string   "peso_cubado"
    t.string   "peso_real_aferido"
    t.string   "peso_faturado"
    t.string   "volumes_consolidados"
    t.string   "meio_transporte"
    t.string   "awb_ctrc"
    t.string   "filler"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "remetentes", force: :cascade do |t|
    t.string   "cnpj"
    t.string   "inscricao_estadual"
    t.string   "endereco"
    t.string   "cidade"
    t.string   "cep"
    t.string   "uf"
    t.string   "data_embarque"
    t.string   "razao_social"
    t.string   "filler"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  create_table "responsavelfretes", force: :cascade do |t|
    t.string   "razao_social"
    t.string   "cnpj"
    t.string   "inscricao_estadual"
    t.string   "endereco"
    t.string   "bairro"
    t.string   "cidade"
    t.string   "cep"
    t.string   "codigo_municipio"
    t.string   "uf"
    t.string   "numero_comunicacao"
    t.string   "filler"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  create_table "valorestotaisdocumentos", force: :cascade do |t|
    t.string   "valor_total_notas"
    t.string   "peso_total_notas"
    t.string   "peso_total_densidade_cubagem"
    t.string   "quantitade_total_volumes"
    t.string   "valor_total_cobrado"
    t.string   "valor_total_seguro"
    t.string   "filler"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  create_table "volumes_nota_fiscals", force: :cascade do |t|
    t.string   "carga_expedicao"
    t.string   "peso"
    t.string   "peos_aferido"
    t.string   "peso_cubado"
    t.string   "comprimento"
    t.string   "largura"
    t.string   "altura"
    t.string   "codigo_barras"
    t.string   "canal_venda"
    t.string   "referencia_parceiro"
    t.string   "filler"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

end
