class CreateConsignatarios < ActiveRecord::Migration[5.0]
  def change
    create_table :consignatarios do |t|
      t.string :razao_social
      t.string :cnpj
      t.string :inscricao_estadual
      t.string :endereco
      t.string :bairro
      t.string :cidade
      t.string :cep
      t.string :codigo_municipio
      t.string :uf
      t.string :numero
      t.string :filler

      t.timestamps
    end
  end
end
