class CreateVolumesNotaFiscals < ActiveRecord::Migration[5.0]
  def change
    create_table :volumes_nota_fiscals do |t|
      t.string :carga_expedicao
      t.string :peso
      t.string :peos_aferido
      t.string :peso_cubado
      t.string :comprimento
      t.string :largura
      t.string :altura
      t.string :codigo_barras
      t.string :canal_venda
      t.string :referencia_parceiro
      t.string :filler

      t.timestamps
    end
  end
end
