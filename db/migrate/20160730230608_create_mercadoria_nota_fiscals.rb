class CreateMercadoriaNotaFiscals < ActiveRecord::Migration[5.0]
  def change
    create_table :mercadoria_nota_fiscals do |t|
      t.string :quantidade_volumes
      t.string :especie_acionamento
      t.string :mercadoria_nota_fiscal
      t.string :quantidade_volumes2
      t.string :especie_acionamento2
      t.string :mercadoria_nota_fiscal2
      t.string :quantidade_volumes3
      t.string :especie_acionamento3
      t.string :mercadoria_nota_fiscal3
      t.string :quantidade_volumes4
      t.string :especie_acionamento4
      t.string :mercadoria_nota_fiscal4
      t.string :filler

      t.timestamps
    end
  end
end
