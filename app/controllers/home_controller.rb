require File.expand_path(File.join(Rails.root, "lib", "core.rb"))

class HomeController < ApplicationController
  def index
  end

  def upload_file
    if params[:file].present?
      Item.new(params[:file].read.lines).parse
    end
    redirect_to root_path
  end

  def erase_all
    Intercambio.delete_all
    Cabecalho.delete_all
    Redespacho.delete_all
    Remetente.delete_all
    Localcoletum.delete_all
    Destinatario.delete_all
    Dadosnotum.delete_all
    Complementonotafiscal.delete_all
    Dadoscomplementare.delete_all
    VolumesNotaFiscal.delete_all
    MercadoriaNotaFiscal.delete_all
    Consignatario.delete_all
    Dadosredespacho.delete_all
    Responsavelfrete.delete_all
    Valorestotaisdocumento.delete_all

    redirect_to root_path
  end
end
