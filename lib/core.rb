class Item
  COD_INTERCAMBIO = "000"
  COD_CABECALHO = "310"
  COD_REDESPACHO = "305"
  COD_REMETENTE = "311"
  COD_LOCAL_COLETA = "309"
  COD_DESTINATARIO = "312"
  COD_DADOS_NOTA = "313"
  COD_COMPLEMENTO_NOTA_FISCAL = "307"
  COD_DADOS_COMPLEMENTARES = "308"
  COD_VOLUMES_NOTA_FISCAL = "319"
  COD_MERCADORIA_NOTA_FISCAL = "314"
  COD_CONSIGNATARIO = "315"
  COD_DADOS_REDESPACHO = "316"
  COD_RESPONSAVEL_FRETE = "317"
  COD_VALORES_TOTAIS_DOCUMENTO = "318"

  attr_reader :lines
  def initialize lines
    @lines = lines
  end

  def parse
    @lines.each do |linha|
      case linha[0..2]
      when COD_INTERCAMBIO then Intercambio.create(remetente: linha[3..37] ,destinatario: linha[38..72], data: linha[73..78], hora: linha[79..82], nome_arquivo: linha[83..94], filler: linha[95..239])
      when COD_CABECALHO then Cabecalho.create(indentificacao_documento: linha[3..16], filler: linha[17..239])
      when COD_REDESPACHO then Redespacho.create(cnpj_emissio: linha[3..16], sigla_fiscal: linha[17..21], numero: linha[22..27], serie: linha[28..30], data_emissao: linha[31..38], total_frete: linha[39..53], frete_peso: linha[54..68], frete_valor: linha[69..83], valor_base_calculo_icms: linha[84..98], valor_icms: linha[99..113], peso_cubado: linha[114..120], peso_real_aferido: linha[121..127], peso_faturado: linha[128..134], volumes_consolidados: linha[135..140], meio_transporte: linha[141..141], awb_ctrc: linha[142..151], filler: linha[152..239])
      when COD_REMETENTE then Remetente.create(cnpj: linha[4..17], inscricao_estadual: linha[17..31], endereco: linha[32..71], cidade: linha[72..106], cep: linha[107..115], uf: linha[116..124], data_embarque: linha[125..132], razao_social: linha[134..173], filler: linha[173..239])
      when COD_LOCAL_COLETA then Localcoletum.create(cnpj: linha[3..16], razao_social: linha[17..56], inscricao_estadual: linha[57..71], endereco_solicitante: linha[72..111], complemento_endereco: linha[112..151], bairro: linha[152..171], cidade: linha[172..211], uf: linha[212..213], pais: linha[214..253], cep: linha[254..261], telefone_fixo: linha[262..296], fax: linha[297..331], telefone_movel: linha[332..366], email: linha[367..426])
      when COD_DESTINATARIO then Destinatario.create(razao_social: linha[3..42], cnpj: linha[43..56], inscricao_estadual: linha[57..71], endereco: linha[72..111], bairro: linha[112..131], cidade: linha[132..166], cep: linha[167..175], codigo_municipio: linha[176..184], uf: linha[185..193], area_frete: linha[194..197], numero_comunicacao: linha[198..232], indentificacao_destinatario: linha[233], filler: linha[234..239], cep_or_zip_code: linha[167..184])
      when COD_DADOS_NOTA then Dadosnotum.create(romaneio_coleta: linha[3..17], cod_rota: linha[18..24], meio_transporte: linha[25], tipo_transporte: linha[26], tipo_carga: linha[27], condicao_frete: linha[28], serie: linha[29..31], numero: linha[32..39], data_emissao: linha[40..47], natureza_mercadoria: linha[48..62], especie_acionamento: linha[63..77], volume: linha[78..84], valor_mercadoria: linha[85..99], peso_total: linha[100..106], peso_densidade_cubagem: linha[107..111], incidencia_icms: linha[112], seguro_efetuado: linha[113], valor_seguro: linha[114..128], valor_a_ser_cobrado: linha[129..143], placa_caminhao: linha[144..150], plano_carga_rapida: linha[151], valor_frete_peso_volume: linha[152..166], valor_ad_valorem: linha[167..181], valor_total_taxas: linha[182..196], valor_total_frete: linha[197..211], acao_documento: linha[212], valor_icms: linha[213..224], valor_icms_retido_subst: linha[225..236], inidicacao_bonificacao: linha[237], filler: linha[238..239])
      when COD_COMPLEMENTO_NOTA_FISCAL then Complementonotafiscal.create(tipo_periodo_entrega: linha[3], data_inicial_entrega: linha[4..11], hora_inicial_entrega: linha[12..15], data_final_entrega: linha[16..23], hora_final_entrega: linha[24..27], uso_cliente: linha[28..67], natureza_nf: linha[28], tipo_entrega: linha[29..32], tipo_carga: linha[33..62], origem: linha[63..66], filler: linha[67], observacao_conhecimento: linha[68..107], pedido: linha[108..127], aliquota_icms: linha[128..131], base_calculo_icms: linha[132..146], cfop_nf: linha[147..150], referencia_entrega: linha[151..239])
      when COD_DADOS_COMPLEMENTARES then Dadoscomplementare.create(email: linha[3..62], celular: linha[63..97], tipo_estabelecimento: linha[98], contribuinte: linha[99], pais: linha[100..139], inscricao_suframa: linha[140..159], validade_insc_suframa: linha[160..167], filler: linha[168..239])
      when COD_VOLUMES_NOTA_FISCAL then VolumesNotaFiscal.create(carga_expedicao: linha[3..22], peso: linha[23..29], peos_aferido: linha[30..36], peso_cubado: linha[37..43], comprimento: linha[44..50], largura: linha[51..57], altura: linha[58..64], codigo_barras: linha[65..104], canal_venda: linha[105..108], referencia_parceiro: linha[109..128], filler: linha[129..239])
      when COD_MERCADORIA_NOTA_FISCAL then MercadoriaNotaFiscal.create(quantidade_volumes: linha[3..9], especie_acionamento: linha[10..24], mercadoria_nota_fiscal: linha[25..54], quantidade_volumes2: linha[55..61], especie_acionamento2: linha[62..76], mercadoria_nota_fiscal2: linha[77..106], quantidade_volumes3: linha[107..113], especie_acionamento3: linha[114..128], mercadoria_nota_fiscal3: linha[129..158], quantidade_volumes4: linha[159..165], especie_acionamento4: linha[166..180], mercadoria_nota_fiscal4: linha[181..210], filler: linha[211..239])
      when COD_CONSIGNATARIO then Consignatario.create(razao_social: linha[3..42], cnpj: linha[43..56], inscricao_estadual: linha[57..71], endereco: linha[72..111], bairro: linha[112..131], cidade: linha[132.166], cep: linha[167..175], codigo_municipio: linha[176..184], uf: linha[185..193], numero: linha[194..228], filler: linha[229..239])
      when COD_DADOS_REDESPACHO then Dadosredespacho.create(razao_social: linha[3..42], cnpj: linha[43..56], inscricao_estadual: linha[57..71], endereco: linha[72..111], bairro: linha[112..131], cidade: linha[132.166], cep: linha[167..175], codigo_municipio: linha[176..184], uf: linha[185..193], area_frete: linha[194..197], numero_comunicacao: linha[198..232], filler: linha[233..239])
      when COD_RESPONSAVEL_FRETE then Responsavelfrete.create(razao_social: linha[3..42], cnpj: linha[43..56], inscricao_estadual: linha[57..71], endereco: linha[72..111], bairro: linha[112..131], cidade: linha[132.166], cep: linha[167..175], codigo_municipio: linha[176..184], uf: linha[185..193], numero_comunicacao: linha[194..228], filler: linha[229..239])
      when COD_VALORES_TOTAIS_DOCUMENTO then Valorestotaisdocumento.create(valor_total_notas: linha[3..17], peso_total_notas: linha[18..32], peso_total_densidade_cubagem: linha[33..47], quantitade_total_volumes: linha[48..62], valor_total_cobrado: linha[63..77], valor_total_seguro: linha[78..92], filler: linha[93..239])
      end
    end
  end
end

