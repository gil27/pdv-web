class CreateDadosnota < ActiveRecord::Migration[5.0]
  def change
    create_table :dadosnota do |t|
      t.string :romaneio_coleta
      t.string :cod_rota
      t.string :meio_transporte
      t.string :tipo_transporte
      t.string :tipo_carga
      t.string :condicao_frete
      t.string :serie
      t.string :numero
      t.string :data_emissao
      t.string :natureza_mercadoria
      t.string :especie_acionamento
      t.string :volume
      t.string :valor_mercadoria
      t.string :peso_total
      t.string :peso_densidade_cubagem
      t.string :incidencia_icms
      t.string :seguro_efetuado
      t.string :valor_seguro
      t.string :valor_a_ser_cobrado
      t.string :placa_caminhao
      t.string :plano_carga_rapida
      t.string :valor_frete_peso_volume
      t.string :valor_ad_valorem
      t.string :valor_total_taxas
      t.string :valor_total_frete
      t.string :acao_documento
      t.string :valor_icms
      t.string :valor_icms_retido_subst
      t.string :inidicacao_bonificacao
      t.string :filler

      t.timestamps
    end
  end
end
