class CreateDestinatarios < ActiveRecord::Migration[5.0]
  def change
    create_table :destinatarios do |t|
      t.string :razao_social
      t.string :cnpj
      t.string :inscricao_estadual
      t.string :endereco
      t.string :bairro
      t.string :cidade
      t.string :cep
      t.string :codigo_municipio
      t.string :uf
      t.string :area_frete
      t.string :numero_comunicacao
      t.string :indentificacao_destinatario
      t.string :filler
      t.string :cep_or_zip_code

      t.timestamps
    end
  end
end
