class CreateComplementonotafiscals < ActiveRecord::Migration[5.0]
  def change
    create_table :complementonotafiscals do |t|
      t.string :tipo_periodo_entrega
      t.string :data_inicial_entrega
      t.string :hora_inicial_entrega
      t.string :data_final_entrega
      t.string :hora_final_entrega
      t.string :uso_cliente
      t.string :natureza_nf
      t.string :tipo_entrega
      t.string :tipo_carga
      t.string :origem
      t.string :filler
      t.string :observacao_conhecimento
      t.string :pedido
      t.string :aliquota_icms
      t.string :base_calculo_icms
      t.string :cfop_nf
      t.string :referencia_entrega

      t.timestamps
    end
  end
end
