class CreateRemetentes < ActiveRecord::Migration[5.0]
  def change
    create_table :remetentes do |t|
      t.string :cnpj
      t.string :inscricao_estadual
      t.string :endereco
      t.string :cidade
      t.string :cep
      t.string :uf
      t.string :data_embarque
      t.string :razao_social
      t.string :filler

      t.timestamps
    end
  end
end
