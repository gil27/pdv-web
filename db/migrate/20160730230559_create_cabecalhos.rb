class CreateCabecalhos < ActiveRecord::Migration[5.0]
  def change
    create_table :cabecalhos do |t|
      t.string :indentificacao_documento
      t.string :filler

      t.timestamps
    end
  end
end
