class CreateValorestotaisdocumentos < ActiveRecord::Migration[5.0]
  def change
    create_table :valorestotaisdocumentos do |t|
      t.string :valor_total_notas
      t.string :peso_total_notas
      t.string :peso_total_densidade_cubagem
      t.string :quantitade_total_volumes
      t.string :valor_total_cobrado
      t.string :valor_total_seguro
      t.string :filler

      t.timestamps
    end
  end
end
