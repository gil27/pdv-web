Rails.application.routes.draw do
  get 'home/index'
  root to: 'home#index'

  post 'upload_file' => 'home#upload_file', as: :upload_file
  get 'erase_all' => 'home#erase_all', as: :erase_all
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
