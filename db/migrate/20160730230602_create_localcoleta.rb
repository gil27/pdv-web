class CreateLocalcoleta < ActiveRecord::Migration[5.0]
  def change
    create_table :localcoleta do |t|
      t.string :cnpj
      t.string :razao_social
      t.string :inscricao_estadual
      t.string :endereco_solicitante
      t.string :complemento_endereco
      t.string :bairro
      t.string :cidade
      t.string :uf
      t.string :pais
      t.string :cep
      t.string :telefone_fixo
      t.string :fax
      t.string :telefone_movel
      t.string :email

      t.timestamps
    end
  end
end
