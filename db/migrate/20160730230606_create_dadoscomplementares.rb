class CreateDadoscomplementares < ActiveRecord::Migration[5.0]
  def change
    create_table :dadoscomplementares do |t|
      t.string :email
      t.string :celular
      t.string :tipo_estabelecimento
      t.string :contribuinte
      t.string :pais
      t.string :inscricao_suframa
      t.string :validade_insc_suframa
      t.string :filler

      t.timestamps
    end
  end
end
