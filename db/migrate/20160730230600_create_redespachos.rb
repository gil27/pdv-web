class CreateRedespachos < ActiveRecord::Migration[5.0]
  def change
    create_table :redespachos do |t|
      t.string :cnpj_emissio
      t.string :sigla_fiscal
      t.string :numero
      t.string :serie
      t.string :data_emissao
      t.string :total_frete
      t.string :frete_peso
      t.string :frete_valor
      t.string :valor_base_calculo_icms
      t.string :valor_icms
      t.string :peso_cubado
      t.string :peso_real_aferido
      t.string :peso_faturado
      t.string :volumes_consolidados
      t.string :meio_transporte
      t.string :awb_ctrc
      t.string :filler

      t.timestamps
    end
  end
end
