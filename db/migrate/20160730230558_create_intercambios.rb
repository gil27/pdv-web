class CreateIntercambios < ActiveRecord::Migration[5.0]
  def change
    create_table :intercambios do |t|
      t.string :remetente
      t.string :destinatario
      t.string :data
      t.string :hora
      t.string :nome_arquivo
      t.string :filler

      t.timestamps
    end
  end
end
